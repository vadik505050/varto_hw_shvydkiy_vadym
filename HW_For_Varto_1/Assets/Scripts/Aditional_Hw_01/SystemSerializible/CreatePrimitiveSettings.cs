using UnityEngine;

[System.Serializable]
public class CreatePrimitiveSettings
{
    public string name;
    public Vector3 position;
    public Vector3 scale;
    public Material material;
}
